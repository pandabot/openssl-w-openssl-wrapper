### A simple wrapper for the openssl binary.

##Features:

- RSA 1024 bit private key generation.
- AES 256 bit (aes-256-cbc) file/text encryption.
- Optional base64 output for encoded text.

##Note:

- ***This script is just a wrapper written in node.js. This script does not provide any additional features outside of the openssl binary.***

##Installation:

```bash
$ git clone https://bitbucket.org/pandabot/openssl-wrapper.git
$ cd openssl-wrapper
$ npm install node-uuid validator
```

##Creating a public/private key pair:

```javascript
openssl.createPrivateKey([bit length], [password], function(err, privateKey) ...
```

- Bit length can be any length supported by openssl (genrsa).
- The password should be passed as a variable and not hard-coded into the script.
- Returns err, if error, or the 'privateKey'.

```javascript
openssl.createPublicKey(privateKey, password, function(err, publicKey) ...
```

- Any RSA private key can be provided here.
- The password should be the same one used to generate the private key, again, this should be passed as a variable and not hard-coded into the script.
- Returns err, if error, or the publicKey

```javascript
openssl.createPrivateKey(1024, 'password', function(err, privateKey){
  if (err) {
    console.log(err);
  } else {
    console.log("PRIVATE KEY\n" + privateKey + "\n");

    openssl.createPublicKey(privateKey, password, function(err, publicKey){
      if (err) {
        console.log(err);
      } else {
        console.log("PUBLIC KEY\n" + publicKey + "\n");
      }
    });
  }
});
```

##Output:
```
PRIVATE KEY
-----BEGIN RSA PRIVATE KEY-----
MIICXQIBAAKBgQCfjzTLyR8Nn0dyz5I4nfV7/4MH/zhr2OIsFzSzkgyxBJJKjfop
Yut9lt4ctozZu2wSEHFZIk4AV7+/1LU8WGDm5U/kJd8YR9Ecs6VKCHpEYbAbdcwi
A53g6gCOA/QmFuJPLWLR+Pv8LDh9hw0Kt5sI7F2xWwW9M+RBSBzoYHw0jQIDAQAB
AoGAAolyW3MdLUhFCIxIVOIeDxSm7q+eJAjUv8K2scw2q3HZKZpMSfAqslp9uZMl
dijGtRrR0or7FoJOqCwnnYgCMmMxsSBhBiYH1GRksO5lpEIq7vRc8MP9Hy6I6EPf
0FF4LwyZLBZenjO7zeDLV78krhl6k/T/IBlisMBGvIy1fnkCQQDK/p4jt0ciQU3Y
nf98DgDQYPGdpJUwlsMzz1Nyxbypdki6DD0Z/qfG33LZ4wicsCqB9+apioyeJDyA
vArqjbMDAkEAyTkeMMxLPKlBSc9xGU2jbiEw5XJziVneaQbGadpXbFDfcNO156kq
iy5jd048dTkud/1FJasS+kkTSD3GgLsdLwJBAK1yAzMnPbtyKw7EaXDZEi76Nlhp
MSRUHTQemQG/HOV68Mcu/+I/KaIOeLbdexVOnPNyGv+QJ3hUI/InOfiR3HsCQGsG
JymbLxUXjLMeUzA7HAaSPrh61Dqkgufng5Z/W+ovZXhUEjchwY//X0XwZbWPcUi+
e42aXOCkB05wFOkZHF8CQQCLiuqCyceh9zX35/yv9kNmaZEhSYgNaQpNYjlAK1Nv
x8gGtR3wT8+qBzSwRa+j45vQJ1hhzCyY/xknscRluycA
-----END RSA PRIVATE KEY-----

PUBLIC KEY
-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCfjzTLyR8Nn0dyz5I4nfV7/4MH
/zhr2OIsFzSzkgyxBJJKjfopYut9lt4ctozZu2wSEHFZIk4AV7+/1LU8WGDm5U/k
Jd8YR9Ecs6VKCHpEYbAbdcwiA53g6gCOA/QmFuJPLWLR+Pv8LDh9hw0Kt5sI7F2x
WwW9M+RBSBzoYHw0jQIDAQAB
-----END PUBLIC KEY-----
```

##Encrypting text:

These functions use the AES method (openssl enc [|-d] -aes-256-cbc) to encrypt or decrypt text.

```javascript
openssl.encryptText([text], [password], [bas64], function(err, encText) ...
```

- [base64] (true|false), true will output the encypted text in base64 format, false will produce binary output.
- Return err, if error, or  the encrypted text.

```javascript
openssl.decryptText(encText, 'password', true, function(err, decText) ...
```

- [base64] (true|false), this should match the format of the encypted text supplied: if you are supplying base64 this option should be true.
- Return err, if error, or the decrypted text.

```javascript
var text = 'This is some text';
var password = 'password';

openssl.encryptText(text, password, true, function(err, encText){
  if (err) {
    console.log(err);
  } else {
    console.log("Encrypted text: " + encText);

    openssl.decryptText(encText, 'password', true, function(err, decText){
      if (err) {
        console.log(err);
      } else {
        console.log("Decrypted text: " + decText);
      }
    });
  }
});
```

##Output:
```
Encrypted text: U2FsdGVkX18FxpQ2Atdb9ggQyrglBN4+/CZ4YGeOcVLEVgXIKXXY6XQ+ke4tuMgE
Decrypted text: This is some text
```

## Encypting text with a public key:

These functions use the public key to encrypt text and the corresponding private key to decrypt the text.

```javascript
openssl.encryptMessage(publicKey, [text], [base64], function(err, encMsg) ...
```

- [base64] (true|false), true will output the encypted text in base64 format, false will produce binary output.
- Return err, if error, or  the encrypted text.

```javascript
openssl.decryptMessage(privateKey, encMsg, true, function(err, decMsg) ...
```

- [base64] (true|false), this should match the format of the encypted text supplied: if you are supplying base64 this option should be true.
- Return err, if error, or the decrypted text.

```javascript
var message = 'test is a message';

openssl.encryptMessage(publicKey, message, true, function(err, encMsg){
  console.log("ENC Msg\n" + encMsg + "\n");

  openssl.decryptMessage(privateKey, encMsg, true, function(err, decMsg){
    console.log("CMD String\n" + decMsg);
  });
});
```

##Output:

```
ENC Msg
SrH9186N8GgqeJPIPRG+JPmuA7CJRI4UWB49oSZTrqg7H8N/BsGwnj8p4Wo1Q1LsWvOni9Rk6DJh
2f6n6UBARMAK5qKy0JXWOYz15CNPrlK/SnhOgI9kbLQ+z3HWhGHw9KVpuPHdswdpbqZttnF+/sVm
31nGodoU1VQ6iQ/o6ak=

DEC Msg
test is a message
```

##Notes

- The spaces, line-breaks and general formatting of the output is very important and should be kept. Failure to do so will result in decryption errors.
- Temporary files are saved to '/dev/shm/' and promptly deleted: it's not recommended to change this location.
- If you have a custom install or location of openssl, you can specify that via:

```javascript
var binary = '/usr/bin/openssl';
```
